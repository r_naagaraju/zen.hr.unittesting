using Microsoft.VisualStudio.TestTools.UnitTesting;
using System;
using System.Reflection;

namespace Zen.Hr.Logic.Tests
{
    [TestClass]
    public class UserServiceTests
    {
        private UserService concern;

        [TestMethod]
        public void Successfully_Returns_All_Users_When_Passing_In_False()
        {
            // unit test goes here

            try
            {
                User u1 = new User();
                u1.UserID = 1;
                u1.UserName = "Active User";
                u1.IsActive = true;

                User u2 = new User();
                u2.UserID = 2;
                u2.UserName = "InActive User";
                u2.IsActive = false;

                User[] ExpectedUsers = new User[2] { u1, u2 };

                UserDataAccess objUserDataAccess = new UserDataAccess();
                concern = new UserService(objUserDataAccess);
                User[] ActualUsers = concern.GetUsers(false);

                Assert.IsNotNull(ActualUsers, "Users object is Null!");
                Assert.IsTrue(ActualUsers.Length > 0, "No User is available");
                Assert.AreEqual(CheckUsers(ExpectedUsers, ActualUsers), true, "Incorrect Users returned.");

            }
            catch (Exception ex)
            {
                string exMsg = string.Empty;
                if (ex.InnerException != null && ex.InnerException.Message.Length > 0)
                    exMsg = ex.InnerException.Message;
                else
                    exMsg = ex.Message;

                throw ex;
            }
        }

        [TestMethod]
        public void Successfully_Returns_All_Active_Users_When_Passing_In_True()
        {
            // unit test goes here
            try
            {
                User u1 = new User();
                u1.UserID = 1;
                u1.UserName = "Active User";
                u1.IsActive = true;

                User[] ExpectedActiveUsers = new User[1] { u1 };

                UserDataAccess objUserDataAccess = new UserDataAccess();
                concern = new UserService(objUserDataAccess);
                User[] ActualActiveUsers = concern.GetUsers(true);

                Assert.IsNotNull(ActualActiveUsers, "Users object is Null!");
                Assert.IsTrue(ActualActiveUsers.Length > 0, "No Active User is available");
                Assert.AreEqual(CheckUsers(ExpectedActiveUsers, ActualActiveUsers), true, "Incorrect Users returned.");

            }
            catch (Exception ex)
            {
                string exMsg = string.Empty;
                if (ex.InnerException != null && ex.InnerException.Message.Length > 0)
                    exMsg = ex.InnerException.Message;
                else
                    exMsg = ex.Message;

                throw ex;
            }
        }

        public static bool CheckUsers(User[] ua, User[] ub)
        {
            if (ua.Length != ub.Length) return false;

            for (int i = 0; i < ua.Length; i++)
            {
                object a = ua[i];
                object b = ub[i];

                Type typeA = a.GetType();
                Type typeB = b.GetType();

                if (typeA != typeB) return false;

                PropertyInfo[] myProperties = typeA.GetProperties(BindingFlags.DeclaredOnly
                                    | BindingFlags.Public | BindingFlags.Instance);

                foreach (PropertyInfo myPropertyA in myProperties)
                {
                    PropertyInfo myPropertyB = typeB.GetProperty(myPropertyA.Name);

                    if (myPropertyA.PropertyType != myPropertyB.PropertyType) return false;
                    if (myPropertyA.GetValue(a, null).ToString() != myPropertyB.GetValue(b, null).ToString()) return false;
                }
            }
            return true;
        }
    }
}
