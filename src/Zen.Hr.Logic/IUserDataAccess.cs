﻿namespace Zen.Hr.Logic
{
    public interface IUserDataAccess
    {
        User[] GetAllUsers();
        User[] GetAllActiveUsers();
    }
    
    public class UserDataAccess : IUserDataAccess
    {
        public UserDataAccess()
        {
        }

        public User[] GetAllUsers()
        {
            // Users has to be read from database. but delcaring locally

            User u1 = new User();
            u1.UserID = 1;
            u1.UserName = "Active User";
            u1.IsActive = true;

            User u2 = new User();
            u2.UserID = 2;
            u2.UserName = "InActive User";
            u2.IsActive = false;

            return new User[2] { u1, u2 };
        }

        public User[] GetAllActiveUsers()
        {
            // Users has to be read from database. but delcaring locally

            User u1 = new User();
            u1.UserID = 1;
            u1.UserName = "Active User";
            u1.IsActive = true;

            return new User[1] { u1 };
        }
    }
}